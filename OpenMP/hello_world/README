== Description ==

This hello world example shows you how to write a simple OpenMP
application and how threads work in OpenMP. You will also learn how to
distinguish between master and worker threads, and how synchronization with
barriers works.

== How to do the exercises ==

*_exercise_*(e.g. c_exercise_hello_world.c) files contain the exercises, please choose a exercise file based
on whichever programming language you are familiar with (c/c++,Fortran).
Open the exercise file, modify it with OpenMP pragma and runtime routines.

[Solution files are also provided. They follow the naming convention *_solution_*.]

== Compilation and running on the training machines with training accounts ==

=== Compilation ===

source build_job.sh <compiler name (cray, intel, gnu, pgi)>

e.g. source build_job.sh cray

=== Running ===

Batch mode(easy to do, same command will work for all machines)
---------------------------------------------------------------
    source run_job.sh <executable name>

    e.g. source run_job.sh c_exercise_hello_world.x 
    
Interactive mode (a bit more work, but more useful for development and testing)
-------------------------------------------------------------------
    For Blue Waters:
    ----------------
    1. Allocate a compute node for 1 hour,
        qsub -I -l nodes=1:ppn=32:xe -l walltime=01:00:00
    2. Once allocated, run the program like any executable
        ./c_exercise_hello_world.x
    For Cori:
    ---------
    1. Allocate a haswell compute node for 1 hour,
        salloc -N 1 -C haswell -p regular -t 1:00:00
    2. Once allocated, run the program like any executable
        ./c_exercise_hello_world.x
    For Edison:
    -----------
    1. Allocate a compute node for 1 hour,
        salloc -N 1 -p regular -t 1:00:00
    2. Once allocated, run the program like any executable
        ./c_exercise_hello_world.x
    For Stampede2:
    --------------
    1. Allocate a KNL compute node for 1 hour,
        idev -p normal -N 1 -n 68 -m 60
    2. Once allocated, run the program like any executable
        ./c_exercise_hello_world.x

====================================================================================

== Compilation and running on other machines or with different accounts ==

=== Compilation ===

make compiler=<comiler name (cray, intel, gnu, pgi)>
e.g. make comiler=cray

=== Running ===

./<executable name>

e.g. ./c_exercise_hello_world.x



