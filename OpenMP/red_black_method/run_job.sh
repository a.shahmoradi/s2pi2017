#!/bin/bash -l
if [ $# -lt 1 ]
then
    echo "no executable is provided, please provide a executable name as parameter"
else
    source ../scripts/run_script.common $1 cpu
fi


