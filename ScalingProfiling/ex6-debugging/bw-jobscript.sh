#!/bin/bash -l
#PBS -l nodes=1:ppn=32:xe
#PBS -l walltime=5:00
#PBS -j oe
#PBS -N ex6

# For this exercise, we have added some errors to our miniFE source code that
# will crash or deadlock the code during the run. We'll run at a high(er) 
# process count than is easy to debug without proper tools, and use ATP to try 
# to find the location of the bug(s)

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
SCRATCH=$HOME/scratch
. /opt/modules/default/init/bash
module swap PrgEnv-cray PrgEnv-intel

module load atp
export ATP_ENABLED=1

# In this exercise, we will run a small miniFE job at a variety of scales
# and plot the time and speedup against the number of MPI ranks. For now,
# we'll stay within a single node.

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex6-debugging/build/miniFE.x

ldd $ex
module list

# A 150x150x150 miniFE job should finish within 5 minutes on a single core 
# of a BW node
sz=150
cmd="$ex -nx $sz -ny $sz -nz $sz"


# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

nranks=$max_mpi_ranks

# make the run output easy to identify:
label=sz${sz}-${PBS_NUM_NODES}n-${nranks}mpi-$PBS_JOBID
# run in a unique directory under $SCRATCH:
rundir=$SCRATCH/$training_dir/ex6-debugging/$label
mkdir -p $rundir
cd $rundir

aprun -n $nranks $cmd > stdout 2> stderr   

