Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 6: Debugging at scale
------------------------------

For this exercise we will build a version of miniFE that has a couple of latent
errors that will cause some ranks to crash or hang. We'll then use ATP to 
capture the state of the hung job in a format that STAT can read, and we'll use 
STAT to find the source of the error.

Note that while STAT is open-source, ATP is part of the Cray software stack, so
it is available on Cori and Blue Waters but not on Stampede. If your local site
has STAT you can use it directly to attach to a hung job, however we won't
cover that here.

.. _Step 1:

Step 1: Build a buggy miniFE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``cd`` to the ``build`` directory and take a look at the Makefile. When the ATP
libraries are linked into an executable, SIGABRT (and optionally SIGTERM) will
trigger a capture of the processes' state for STAT to read later. On a Cray,
having the ``atp`` module loaded into your environment will cause the compiler 
wrappers to add ATP support. On Cori, the ``atp`` module is loaded by default.

Make sure ATP is loaded and build the executable::

  $ module load atp
  $ make

When running the job, you might find it is difficult to catch and kill the hung 
job before it times out. You can ensure the job crashes instead of hanging by 
going to the bottom of ``build/Makefile`` and selecting the CPPFLAGS line::

  CPPFLAGS += -DSTPI_FAKE_ERROR_1=20
  # comment the others out:
  #CPPFLAGS += -DSTPI_FAKE_ERROR_2=20
  #CPPFLAGS += -DSTPI_FAKE_ERROR_1=20 -DSTPI_FAKE_ERROR_2=20

Then rebuild the executable::

  $ make clean
  $ make

.. _Step 2:

Step 2: Run the job and watch for problems
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To enable ATP you must ensure the module is loaded into your run environment
and set ATP_ENABLED=1.

Notice that we have set quite a short time limit for this job - you may wish 
to make it a little longer if you find that it times out before you've managed 
to send the signal

Depending on which bug you hit, the job should either crash or hang within a
minute or so. You can check the ``stdout`` file in the run directory, if it 
appears to be making no progress, miniFE has probably hung::

  $ sbatch cori-jobscript.sh
  Submitted batch job 5498106
  $ sqs
  JOBID              ST   REASON       USER         NAME         NODES        USED         REQUESTED    SUBMIT                PARTITION    RANK_P       RANK_BF
  5498106            R    None         sleak        cori-jobscri 2            2:42         5:00         2017-06-24T14:29:20   debug        N/A          N/A
  $ cat $SCRATCH/path/to/run/dir/stdout
  making matrix indices local...0.186606s, total time: 1.11898
  Starting CG solver ...
  Initial Residual = 201.002
  Iteration = 20   Residual = 0.0890651

Note that your /path/to/run/dir will probably look something like::

  $SCRATCH/Training/s2pi2017/ScalingProfiling/ex6-debugging/sz200-2n-64mpi-5498106

It's been stuck at this iteration for a while, so it has probably hung. (But 
remember that it might also crash, in which case the job will disappear from 
``sqs`` output)

To trigger ATP on a hung job we need to send it an ABRT signal (see ``kill -l``
for a list of available signals). On Cori the login nodes can't see the compute
nodes, so you'll need to send the signal from a MOM node, either ``cmom02`` or
``cmom05``. You want to signal the actual job step (``12345.0`` for the first 
``srun`` in job ``12345``). And the way to send the signal is with 
``scancel -s ABRT``. Putting all of that together::

  $ ssh cmom02 /usr/bin/scancel -s ABRT 5498106.0

On Blue Waters you can send the signal by finding your job's "apid" and passing
it to ``apkill``::

  $ apstat | grep $USER
  $ # the first column is the apid
  $ apkill <apid>

It will take a few minutes for the STAT information to be gathered, if you go 
to your run directory and look at ``stderr`` you should see messages like::

  $ cat stderr
  Application 5498106 is crashing. ATP analysis proceeding...

  ATP Stack walkback for Rank 32 starting:
    _start@start.S:122
    __libc_start_main@libc-start.c:285
  ...

And afterwards will appear 2 files: ``atpMergedBT.dot`` and 
``atpMergedBT_line.dot``. The ``_line`` version has source line number data 
added, which give a more verbose - but perhaps more helpful - output.


.. _Step 3:

Step 3: Use STAT to help find where each process was stopped
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We'll look at the result with ``stat-view`` which is an X application, so 
you'll need to have logged in with X forwarding, or via NX_ (on Cori).

.. _NX: http://www.nersc.gov/users/connecting-to-nersc/using-nx/

Make sure STAT is loaded into your environment then use ``stat-view`` to look
at ``atpMergedBT_line.dot``::

  $ module load stat
  $ stat-view atpMergedBT_line.dot

We'll go over the ``stat-view`` GUI in the slides, in the meantime, browse the 
GUI and see if it leads to a location in the miniFE source code where the bug
was added.

