#!/bin/bash -l
#SBATCH -N 1
#SBATCH -t 5
#SBATCH -L SCRATCH
#SBATCH -C haswell
#SBATCH -J ex2
##SBATCH --reservation=STPI_hsw

# In this exercise, we will run our TAU-instrumented miniFE job at a couple of
# scales and use TAU to identify which aspects of the code contributed most 
# to poor scaling

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}

module load tau

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex2-profiling/build/miniFE-tau.x

# A 200x200x200 miniFE job should finish within 5 minutes on a single core 
# of a Cori Haswell node:
sz=200
cmd="$ex -nx $sz -ny $sz -nz $sz"

# A Cori Haswell node has 32 cores, each with 2 hyperthreads. Our Slurm 
# setup considers each hyperthread to be a CPU, but for this exercise we want
# each MPI task to have its own core. The following recipe calculates how
# many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
# a "cpu" in Slurm is a hyperthread:
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((SLURM_CPUS_ON_NODE/cpus_per_task))
max_mpi_ranks=$((SLURM_NNODES * max_mpi_per_node))

# we will still time the overall run, to get a sense of how much overhead 
# profiling added. We'll output the time in seconds to be consistent with ex1
TIMEFORMAT=%R

# a report of communication between each pair of ranks is interesting:
export TAU_COMM_MATRIX=1

# let's look at the profile with 1, 2, 16 and 32 MPI processes:
for nranks in 1 2 16 32 ; do

  # make the run output easy to identify:
  label=tau_profile-sz${sz}-${SLURM_NNODES}n-${nranks}mpi-$SLURM_JOB_ID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex2-profiling/$label
  mkdir -p $rundir
  cd $rundir

  echo "profiling miniFE with $nranks MPI processes on $SLURM_NNODES nodes at `date`"
  time srun -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr

done
echo "finished miniFE runs at `date`"
