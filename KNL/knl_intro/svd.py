#!/usr/bin/env python
"""
/*
 * =====================================================================================
 *
 *       Filename:  svd.py
 *
 *    Description:  SVD using numpy
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */
"""
import time
import numpy as np

np.random.seed(seed = 1)
A = np.random.randn(2000,2000)

start = time.time()
U, s, V = np.linalg.svd(A, full_matrices = True)
end = time.time()
print 'runtime = ' + str(end-start) +'s'
